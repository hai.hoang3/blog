export default {
    computed: {
        filteredBlogs:function(){
            return this.blogs.filter((blog) => {
                // Function check character in js
                return blog.title.match(this.search);
            });
        }
    }
}