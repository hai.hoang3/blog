import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App.vue'
import VueRouter from 'vue-router';
import Routes from './routes';

Vue.config.productionTip = false

// Use router
Vue.use(VueRouter);

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
})

// Use vue-resource package
Vue.use(VueResource);

// use Custom Directives create event
// Vue.directive('rainbow', {
//   bind(el){
//       el.style.color = "#" + Math.random().toString(16).slice(2, 8);
//   }
// });

// Use filter 
Vue.filter('to-uppercase', function(value){
  return value.toUpperCase();
})

Vue.filter('snipper', function(value) {
  return value.slice(0,50);
})

new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
